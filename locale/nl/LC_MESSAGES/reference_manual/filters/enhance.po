# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-12 14:19+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/filters/enhance.rst:1
msgid "Overview of the enhance filters."
msgstr "Overzicht van de verbeterfilters."

#: ../../reference_manual/filters/enhance.rst:10
#: ../../reference_manual/filters/enhance.rst:19
msgid "Sharpen"
msgstr "Verscherpen"

#: ../../reference_manual/filters/enhance.rst:10
msgid "Filters"
msgstr "Filters"

#: ../../reference_manual/filters/enhance.rst:15
msgid "Enhance"
msgstr "Verbeteren"

#: ../../reference_manual/filters/enhance.rst:17
msgid ""
"These filters all focus on reducing the blur in the image by sharpening and "
"enhancing details and the edges. Following are various sharpen and enhance "
"filters in provided in Krita."
msgstr ""
"Deze filters richten zich op het verminderen van vervaging in de afbeelding "
"door verscherpen en verhogen van details en de randen. Verder zijn er "
"verschillende filter voor verscherpen en verbeteren meegeleverd in Krita."

#: ../../reference_manual/filters/enhance.rst:20
msgid "Mean Removal"
msgstr "Gemiddeldenverwijdering"

#: ../../reference_manual/filters/enhance.rst:21
msgid "Unsharp Mask"
msgstr "Onscherp masker"

#: ../../reference_manual/filters/enhance.rst:22
msgid "Gaussian Noise reduction"
msgstr "Gaussiaanse ruisverwijdering"

#: ../../reference_manual/filters/enhance.rst:23
msgid "Wavelet Noise Reducer"
msgstr "Wavelet-ruisverwijdering"
