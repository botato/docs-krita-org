msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../reference_manual/layers_and_masks/filter_layers.rst:1
msgid "How to use filter layers in Krita."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_layers.rst:14
msgid "Layers"
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_layers.rst:14
msgid "Filters"
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_layers.rst:19
msgid "Filter Layer"
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_layers.rst:22
msgid ""
"Filter layers show whatever layers are underneath them, but with a filter "
"such as Layer Styles, Blur, Levels, Brightness / Contrast. For example, if "
"you add a **Filter Layer**, and choose the Blur filter, you will see every "
"layer under your filter layer blurred."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_layers.rst:24
msgid ""
"Unlike applying a filter directly on to a section of a Paint Layer, Filter "
"Layers do not actually alter the original image in the Paint Layers below "
"them. Once again, non-destructive editing! You can tweak the filter at any "
"time, and the changes can always be altered or removed."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_layers.rst:26
msgid ""
"Unlike Filter Masks though, Filter Layers apply to the entire canvas for the "
"layers beneath. If you wish to apply a filter layer to only *some* layers, "
"then you can utilize the Group Layer feature and add those layers into a "
"group with the filter layer on top of the stack."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_layers.rst:28
msgid ""
"You can edit the settings for a filter layer, by double clicking on it in "
"the Layers docker."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_layers.rst:31
msgid ""
"Only Krita native filters (the ones in the :guilabel:`Filters` menu) can be "
"used with Filter Layers. Filter Layers are not supported using the "
"externally integrated G'Mic filters."
msgstr ""
