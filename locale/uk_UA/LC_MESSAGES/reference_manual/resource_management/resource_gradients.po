# Translation of docs_krita_org_reference_manual___resource_management___resource_gradients.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___resource_management___resource_gradients\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:36+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "HSV counter-clock wise."
msgstr "HSV проти годинникової стрілки."

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_new_gradient.png"
msgstr ".. image:: images/gradients/Krita_new_gradient.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_stop_gradient.png"
msgstr ".. image:: images/gradients/Krita_stop_gradient.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_move_stop.png"
msgstr ".. image:: images/gradients/Krita_move_stop.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_stop_sudden_change.png"
msgstr ".. image:: images/gradients/Krita_stop_sudden_change.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_segment_gradient_options.png"
msgstr ".. image:: images/gradients/Krita_segment_gradient_options.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_gradient_segment_blending.png"
msgstr ".. image:: images/gradients/Krita_gradient_segment_blending.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:None
msgid ".. image:: images/gradients/Krita_gradient_hsv_cw.png"
msgstr ".. image:: images/gradients/Krita_gradient_hsv_cw.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:1
msgid "Creating and managing gradients in Krita."
msgstr "Створення градієнтів і керування градієнтами у Krita."

#: ../../reference_manual/resource_management/resource_gradients.rst:11
#: ../../reference_manual/resource_management/resource_gradients.rst:16
msgid "Gradients"
msgstr "Градієнти"

#: ../../reference_manual/resource_management/resource_gradients.rst:11
msgid "Resources"
msgstr "Ресурси"

#: ../../reference_manual/resource_management/resource_gradients.rst:19
msgid "Accessing a Gradient"
msgstr "Доступ до градієнта"

#: ../../reference_manual/resource_management/resource_gradients.rst:21
msgid ""
"The Gradients configuration panel is accessed by clicking the Gradients icon "
"(usually the icon next to the disk)."
msgstr ""
"Відкрити панель налаштовування градієнтів можна натисканням піктограми "
"градієнтів (зазвичай, піктограми, яку розташовано поряд із піктограмою "
"диска)."

#: ../../reference_manual/resource_management/resource_gradients.rst:24
msgid ".. image:: images/gradients/Gradient_Toolbar_Panel.png"
msgstr ".. image:: images/gradients/Gradient_Toolbar_Panel.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:25
msgid ""
"Gradients are configurations of blending between colors.  Krita provides "
"over a dozen preset dynamic gradients for you to choose from.  In addition, "
"you can design and save your own."
msgstr ""
"Градієнти — налаштування переходу між кольорами. У Krita передбачено з "
"десяток готових динамічних градієнтів — ви можете вибрати будь-який. Крім "
"того, ви можете створювати і зберігати власні градієнти."

#: ../../reference_manual/resource_management/resource_gradients.rst:27
msgid "Some typical uses of gradients are:"
msgstr "Деякі з типових випадків використання градієнтів:"

#: ../../reference_manual/resource_management/resource_gradients.rst:29
msgid "Fill for vector shapes."
msgstr "Заповнення векторних форм."

#: ../../reference_manual/resource_management/resource_gradients.rst:30
msgid "Gradient tool"
msgstr "Інструмент «Градієнт»"

#: ../../reference_manual/resource_management/resource_gradients.rst:31
msgid "As a source of color for the pixel brush."
msgstr "Джерело кольору для піксельних пензлів."

#: ../../reference_manual/resource_management/resource_gradients.rst:33
msgid ""
"There is no gradients docker. They can only be accessed through the gradient "
"\"quick-menu\" in the toolbar."
msgstr ""
"Спеціальної бічної панелі градієнтів не передбачено. Доступ до градієнтів "
"можна здійснювати лише за допомогою «швидкого» меню градієнтів на панелі "
"інструментів."

#: ../../reference_manual/resource_management/resource_gradients.rst:36
msgid "Editing a Gradient"
msgstr "Редагування градієнта"

#: ../../reference_manual/resource_management/resource_gradients.rst:38
msgid "Krita has two gradient types:"
msgstr "У Krita передбачено два типи градієнтів:"

#: ../../reference_manual/resource_management/resource_gradients.rst:40
msgid ""
"Segmented Gradients, which are compatible with GIMP, have many different "
"features but are also a bit complicated to make."
msgstr ""
"Сегментовані градієнти, які сумісні з GIMP, дуже гнучкі у налаштовуванні, "
"але дещо складні у створенні."

#: ../../reference_manual/resource_management/resource_gradients.rst:41
msgid ""
"Stop Gradients, which are saved as SVG files and similar to how most "
"applications do their gradients, but has less features than the segmented "
"gradient."
msgstr ""
"Градієнти із контрольними точками, які зберігаються у форматі файлів SVG і "
"подібні до тих градієнтів, які можна зустріти у більшості інших програм. "
"Втім, їхні можливості не такі широкі як у сегментованих градієнтів."

#: ../../reference_manual/resource_management/resource_gradients.rst:43
msgid ""
"Initially we could only make segmented gradients in Krita, but in 3.0.2 we "
"can also make stop gradients."
msgstr ""
"У початкових версіях Krita було передбачено лише сегментовані градієнти, "
"але, починаючи з версії 3.0.2, у програмі реалізовано підтримку градієнтів "
"із контрольними точками."

#: ../../reference_manual/resource_management/resource_gradients.rst:48
msgid ""
"You can make a new gradient by going into the drop-down and selecting the "
"gradient type you wish to have. By default Krita will make a stop-gradient."
msgstr ""
"Ви можете створити градієнт за допомогою спадного списку. Виберіть тип "
"градієнта, який вам потрібен. Типово, Krita створить градієнт із "
"контрольними точками."

#: ../../reference_manual/resource_management/resource_gradients.rst:51
msgid "Stop Gradients"
msgstr "Градієнти із контрольними точками"

#: ../../reference_manual/resource_management/resource_gradients.rst:56
msgid "Stop gradients are very straight forward:"
msgstr "Градієнти із контрольними точками працюють дуже просто:"

#: ../../reference_manual/resource_management/resource_gradients.rst:58
msgid "|mouseleft| on the gradient to add a stop."
msgstr "Клацніть |mouseleft| на градієнті, щоб додати контрольну точку."

#: ../../reference_manual/resource_management/resource_gradients.rst:59
msgid "|mouseleft| on the stops to select them, and drag to move them."
msgstr ""
"Клацніть |mouseleft| на контрольній точці, щоб позначити її, і перетягніть "
"її, щоб змінити її розташування."

#: ../../reference_manual/resource_management/resource_gradients.rst:60
msgid ""
"|mouseright| on the stops to remove them. A stop gradient will not allow you "
"to remove stops if there's only two left."
msgstr ""
"Клацніть |mouseright| на контрольній точці, щоб вилучити її. Засоби "
"керування градієнтом із контрольними точками запобігатимуть вилученню точки, "
"якщо на градієнті лишиться лише дві точки."

#: ../../reference_manual/resource_management/resource_gradients.rst:65
msgid ""
"A selected stop can have its color and transparency changed using the color "
"button and the opacity slider below."
msgstr ""
"Для позначеної контрольної точки можна змінити колір і прозорість за "
"допомогою кнопки вибору кольору та повзунка непрозорості."

#: ../../reference_manual/resource_management/resource_gradients.rst:70
msgid ""
"As per SVG spec, you can make a sudden change between stops by moving them "
"close together. The stops will overlap, but you can still drag them around."
msgstr ""
"Відповідно до специфікації SVG ви можете створювати різку зміну між "
"контрольними точками, пересунувши їх одна до одної. Контрольні точки "
"накладуться одна на одну, але ви все одно можете їх розтягнути."

#: ../../reference_manual/resource_management/resource_gradients.rst:73
msgid "Segmented Gradients"
msgstr "Сегментовані градієнти"

#: ../../reference_manual/resource_management/resource_gradients.rst:76
msgid ".. image:: images/gradients/Krita_Editing_Custom_Gradient.png"
msgstr ".. image:: images/gradients/Krita_Editing_Custom_Gradient.png"

#: ../../reference_manual/resource_management/resource_gradients.rst:77
msgid ""
"Segmented gradients are a bit more tricky. Instead of going from color to "
"color, it allows you to define segments, which each can have a begin and end "
"color."
msgstr ""
"Сегментовані градієнти є дещо складнішими. Замість переходу від кольору до "
"кольору, програма надає вам змогу визначити сегменти, кожен з яких може мати "
"власний початковий і кінцевий колір."

#: ../../reference_manual/resource_management/resource_gradients.rst:79
msgid "|mouseright| the gradient to call up this menu:"
msgstr "Клацніть |mouseright| на градієнті, щоб викликати таке меню:"

#: ../../reference_manual/resource_management/resource_gradients.rst:84
msgid "Split Segment"
msgstr "Розділити відрізок"

#: ../../reference_manual/resource_management/resource_gradients.rst:85
msgid ""
"This splits the current segment in two, using the white arrow, the segment "
"middle as the place to split. It will also use the color at the white arrow "
"to define the new colors in place in the new segments."
msgstr ""
"Розділити поточний сегмент на два за допомогою білої стрілочки. Місцем "
"поділу буде середина сегмента. Також програма використає колір у позиції "
"білої стрілочки для визначення нових кольорів для нових сегментів."

#: ../../reference_manual/resource_management/resource_gradients.rst:86
msgid "Duplicate segment"
msgstr "Здублювати відрізок"

#: ../../reference_manual/resource_management/resource_gradients.rst:87
msgid ""
"Similar to split, but instead the two new segments are copies of the old one."
msgstr ""
"Подібне до поділу, але два сегменти, які буде утворено, будуть копіями "
"оригіналу."

#: ../../reference_manual/resource_management/resource_gradients.rst:88
msgid "Mirror segment"
msgstr "Дзеркально скопіювати відрізок"

#: ../../reference_manual/resource_management/resource_gradients.rst:89
msgid "Mirrors the segment colors."
msgstr "Віддзеркалює кольори відрізка."

#: ../../reference_manual/resource_management/resource_gradients.rst:91
msgid "Remove segment"
msgstr "Вилучити відрізок"

#: ../../reference_manual/resource_management/resource_gradients.rst:91
msgid "Removes the segment."
msgstr "Вилучає сегмент."

#: ../../reference_manual/resource_management/resource_gradients.rst:93
msgid ""
"|mouseleft| + dragging the black arrows will resize the segments attaching "
"to those arrows. |mouseleft| + dragging the white arrows will change the mid "
"point of that segment, changing the way how the mixture is made."
msgstr ""
"Натискання |mouseleft| + перетягування чорних стрілочок змінює розміри "
"сегментів, які пов'язано із цими стрілочками. Натискання |mouseleft| + "
"перетягування білих стрілочок змінює розташування середньої точки сегмента, "
"змінюючи спосіб, у який виконується змішування."

#: ../../reference_manual/resource_management/resource_gradients.rst:95
msgid ""
"At the bottom, you can set the color and transparency of either part of the "
"segment."
msgstr ""
"Знизу ви можете встановити колір і прозорість для усіх частин сегмента."

#: ../../reference_manual/resource_management/resource_gradients.rst:97
msgid "You can also set the blending. The first is the interpolation mode:"
msgstr ""
"Ви також можете встановити режим змішування. Першим є режим інтерполяції:"

#: ../../reference_manual/resource_management/resource_gradients.rst:102
msgid "Linear - Does a linear blending between both segments."
msgstr "Лінійне — виконати лінійне змішування між сегментами."

#: ../../reference_manual/resource_management/resource_gradients.rst:103
msgid "Curved - This causes the mix to ease-in and out faster."
msgstr "Викривлене — швидше наростання і спадання змішування."

#: ../../reference_manual/resource_management/resource_gradients.rst:104
msgid ""
"Sine - Uses a sine function. This causes the mix to ease in and out slower."
msgstr ""
"Синус — використати функцію синуса. Уповільнює наростання та спадання "
"змішування."

#: ../../reference_manual/resource_management/resource_gradients.rst:105
msgid ""
"Sphere, increasing - This puts emphasis on the later color during the mix."
msgstr "Сферичне, збільшення — акцент на другий колір під час змішування."

#: ../../reference_manual/resource_management/resource_gradients.rst:106
msgid ""
"Sphere, decreasing - This puts emphasis on the first color during the mix."
msgstr "Сферичне, зменшення — акцент на перший колір під час змішування."

#: ../../reference_manual/resource_management/resource_gradients.rst:108
msgid "Finally, there's the model:"
msgstr "Нарешті, передбачено модель:"

#: ../../reference_manual/resource_management/resource_gradients.rst:113
msgid "RGB"
msgstr "RGB"

#: ../../reference_manual/resource_management/resource_gradients.rst:114
msgid "Does the blending in RGB model."
msgstr "Виконує змішування у моделі RGB."

#: ../../reference_manual/resource_management/resource_gradients.rst:115
msgid "HSV clockwise"
msgstr "HSV (відтінок за стрілкою годинника)"

#: ../../reference_manual/resource_management/resource_gradients.rst:116
msgid ""
"Blends the two colors using the HSV model, and follows the hue clockwise "
"(red-yellow-green-cyan-blue-purple). The above screenshot is an example of "
"this."
msgstr ""
"Змішує два кольори на основі моделі HSV із переходом за відтінком за "
"годинниковою стрілкою (червоний-жовтий-зелений-блакитний-синій-пурпуровий). "
"Приклад наведено на знімку вікна вище."

#: ../../reference_manual/resource_management/resource_gradients.rst:118
msgid "Blends the color as the previous options, but then counter-clockwise."
msgstr ""
"Змішування кольору, як і у попередніх варіантах, але проти годинникової "
"стрілки."
