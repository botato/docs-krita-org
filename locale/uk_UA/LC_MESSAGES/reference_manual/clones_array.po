# Translation of docs_krita_org_reference_manual___clones_array.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___clones_array\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-05-06 22:06+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<generated>:1
msgid "Angle"
msgstr "Кут"

#: ../../reference_manual/clones_array.rst:1
msgid "The Clones Array functionality in Krita"
msgstr "Функціональна можливість створення масиву клонів у Krita"

#: ../../reference_manual/clones_array.rst:10
#: ../../reference_manual/clones_array.rst:15
msgid "Clones Array"
msgstr "Масив клонів"

#: ../../reference_manual/clones_array.rst:10
msgid "Clone"
msgstr "Клон"

#: ../../reference_manual/clones_array.rst:17
msgid ""
"Allows you to create a set of clone layers quickly. These are ordered in "
"terms of rows and columns. The default options will create a 2 by 2 grid. "
"For setting up tiles of an isometric game, for example, you'd want to set "
"the X offset of the rows to half the value input into the X offset for the "
"columns, so that rows are offset by half. For a hexagonal grid, you'd want "
"to do the same, but also reduce the Y offset of the grids by the amount of "
"space the hexagon can overlap with itself when tiled."
msgstr ""
"Надає вам змогу швидко створювати набори клонованих шарів. Клони "
"впорядковано за рядками і стовпчиками. За типових значень параметрів "
"програма створює таблицю клонів 2 на 2. Для створення плиток у ізометричній "
"грі, наприклад, вам варто встановити зміщення за віссю X для рядків у "
"половину значення відступу за віссю X для стовпчиків, щоб зміщення за "
"рядками було половинним. Для шестикутної ґратки слід зробити те саме, але ще "
"й зменшити зміщення за віссю Y у ґратці на ширину перекриття шестикутників у "
"мозаїці ґратки."

#: ../../reference_manual/clones_array.rst:19
msgid "\\- Elements"
msgstr "\\- елементи"

#: ../../reference_manual/clones_array.rst:20
msgid ""
"The amount of elements that should be generated using a negative of the "
"offset."
msgstr "Кількість елементів, які має бути створено із від'ємним зміщенням."

#: ../../reference_manual/clones_array.rst:21
msgid "\\+ Elements"
msgstr "\\+ елементи"

#: ../../reference_manual/clones_array.rst:22
msgid ""
"The amount of elements that should be generated using a positive of the "
"offset."
msgstr "Кількість елементів, які має бути створено із додатним зміщенням."

#: ../../reference_manual/clones_array.rst:23
msgid "X offset"
msgstr "Зміщення за X"

#: ../../reference_manual/clones_array.rst:24
msgid ""
"The X offset in pixels. Use this in combination with Y offset to position a "
"clone using Cartesian coordinates."
msgstr ""
"Зміщення за віссю X у пікселях. Скористайтеся цим пунктом у поєднанні із "
"пунктом зміщення за Y для визначення розташування клону у декартових "
"координатах."

#: ../../reference_manual/clones_array.rst:25
msgid "Y offset"
msgstr "Зміщення за Y"

#: ../../reference_manual/clones_array.rst:26
msgid ""
"The Y offset in pixels. Use this in combination with X offset to position a "
"clone using Cartesian coordinates."
msgstr ""
"Зміщення за віссю Y у пікселях. Скористайтеся цим пунктом у поєднанні із "
"пунктом зміщення за X для визначення розташування клону у декартових "
"координатах."

#: ../../reference_manual/clones_array.rst:27
msgid "Distance"
msgstr "Відстань"

#: ../../reference_manual/clones_array.rst:28
msgid ""
"The line-distance of the original origin to the clones origin. Use this in "
"combination with angle to position a clone using a polar coordinate system."
msgstr ""
"Лінійна відстань початкового початку координат від початку координат клону. "
"Скористайтеся цим пунктом у поєднанні із кутом для визначення розташування "
"клону у полярній системі координат."

#: ../../reference_manual/clones_array.rst:30
msgid ""
"The angle-offset of the column or row. Use this in combination with distance "
"to position a clone using a polar coordinate system."
msgstr ""
"Кутове зміщення стовпчика або рядка. Скористайтеся цим пунктом у поєднанні "
"із пунктом відстані для визначення розташування клону у полярній системі "
"координат."
