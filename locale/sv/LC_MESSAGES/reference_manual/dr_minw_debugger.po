# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:09+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-crash-screen.png"
msgstr ".. image:: images/Mingw-crash-screen.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-explorer-path.png"
msgstr ".. image:: images/Mingw-explorer-path.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-crash-log-start.png"
msgstr ".. image:: images/Mingw-crash-log-start.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-crash-log-end.png"
msgstr ".. image:: images/Mingw-crash-log-end.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-dbg7zip.png"
msgstr ".. image:: images/Mingw-dbg7zip.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-dbg7zip-dir.png"
msgstr ".. image:: images/Mingw-dbg7zip-dir.png"

#: ../../reference_manual/dr_minw_debugger.rst:1
msgid "How to get a backtrace in Krita using the dr. MinW debugger."
msgstr "Hur man får en bakåtspårning i Krita med avlusaren dr. MinW"

#: ../../reference_manual/dr_minw_debugger.rst:13
msgid "Backtrace"
msgstr "Bakåtspårning "

#: ../../reference_manual/dr_minw_debugger.rst:13
msgid "Debug"
msgstr "Avlusning"

#: ../../reference_manual/dr_minw_debugger.rst:18
msgid "Dr. MinW Debugger"
msgstr "Avlusaren Dr. MinW"

#: ../../reference_manual/dr_minw_debugger.rst:22
msgid ""
"The information on this page applies only to the Windows release of Krita "
"3.1 Beta 3 (3.0.92) and later."
msgstr ""
"Informationen på den här sidan gäller bara utgåvor av Krita 3.1 Beta 3 "
"(3.0.92) eller senare på Windows."

#: ../../reference_manual/dr_minw_debugger.rst:26
msgid "Getting a Backtrace"
msgstr "Skapa en bakåtspårning"

#: ../../reference_manual/dr_minw_debugger.rst:28
msgid ""
"There are some additions to Krita which makes getting a backtrace much "
"easier on Windows."
msgstr ""
"Det finns några tillägg i Krita som gör det mycket enklare att skapa en "
"bakåtspårning på Windows."

#: ../../reference_manual/dr_minw_debugger.rst:32
msgid ""
"When there is a crash, Krita might appear to be unresponsive for a short "
"time, ranging from a few seconds to a few minutes, before the crash dialog "
"appears."
msgstr ""
"När en krasch inträffar, kan det verka som om Krita inte svarar en kort "
"stund, från några få sekunder till några få minuter, innan kraschdialogrutan "
"visas."

#: ../../reference_manual/dr_minw_debugger.rst:36
msgid "An example of the crash dialog."
msgstr "Ett exempel på kraschdialogrutan."

#: ../../reference_manual/dr_minw_debugger.rst:38
msgid ""
"If Krita keeps on being unresponsive for more than a few minutes, it might "
"actually be locked up, which may not give a backtrace. In that situation, "
"you have to close Krita manually. Continue to follow the following "
"instructions to check whether it was a crash or not."
msgstr ""
"Om Krita fortsätter att inte svara efter mer en några minuter, kan det i "
"själva verket vara helt låst, vilket kan leda till att ingen bakåtspårning "
"skapas. I sådana situationer, måste Krita avslutas manuellt. Fortsätt att "
"följa nedanstående instruktioner för att kontrollera om det var en krasch "
"eller inte."

#: ../../reference_manual/dr_minw_debugger.rst:40
msgid ""
"Open Windows Explorer and type ``%LocalAppData%`` (without quotes) on the "
"address bar and press the :kbd:`Enter` key."
msgstr ""
"Öppna Windows Utforskare och skriv ``%LocalAppData%`` (utan citationstecken) "
"i adressraden och tryck på tangenten :kbd:`Enter`."

#: ../../reference_manual/dr_minw_debugger.rst:44
msgid ""
"Find the file ``kritacrash.log`` (it might appear as simply ``kritacrash`` "
"depending on your settings.)"
msgstr ""
"Leta rätt på filen ``kritacrash.log`` (den kan visas som bara ``kritacrash`` "
"beroende på inställningarna.)"

#: ../../reference_manual/dr_minw_debugger.rst:45
msgid ""
"Open the file with Notepad and scroll to the bottom, then scroll up to the "
"first occurrence of “Error occurred on <time>” or the dashes."
msgstr ""
"Öppna filen med Anteckningar och gå längst ner, gå därefter uppåt till "
"första förekomsten av “Error occurred on <tid>” eller bindestrecken."

#: ../../reference_manual/dr_minw_debugger.rst:49
msgid "Start of backtrace."
msgstr "Bakåtspårningens start."

#: ../../reference_manual/dr_minw_debugger.rst:51
msgid "Check the time and make sure it matches the time of the crash."
msgstr "Kontrollera tiden och säkerställ att den motsvarar tiden för kraschen."

#: ../../reference_manual/dr_minw_debugger.rst:55
msgid "End of backtrace."
msgstr "Bakåtspårningens slut."

#: ../../reference_manual/dr_minw_debugger.rst:57
msgid ""
"The text starting from this line to the end of the file is the most recent "
"backtrace."
msgstr ""
"Texten som börjar med den här raden till slutet på filen är den senaste "
"bakåtspårningen."

#: ../../reference_manual/dr_minw_debugger.rst:59
msgid ""
"If ``kritacrash.log`` does not exist, or a backtrace with a matching time "
"does not exist, then you don’t have a backtrace. This means Krita was very "
"likely locked up, and a crash didn’t actually happen. In this case, make a "
"bug report too."
msgstr ""
"Om ``kritacrash.log`` inte finns, eller en bakåtspårning med en motsvarande "
"tid inte finns, har man inte någon bakåtspårning. Det betyder troligtvis att "
"Krita låstes helt, och att en krasch faktiskt inte inträffade. Skapa också "
"en felrapport i detta fall."

#: ../../reference_manual/dr_minw_debugger.rst:60
msgid ""
"If the backtrace looks truncated, or there is nothing after the time, it "
"means there was a crash and the crash handler was creating the stack trace "
"before being closed manually. In this case, try to re-trigger the crash and "
"wait longer until the crash dialog appears."
msgstr ""
"Om bakåtspårningen ser avkortad ut, eller det inte finns något efter tiden, "
"betyder det att en krasch inträffade och att kraschhanteraren höll på att "
"skapa bakåtspårningen innan den avslutades manuellt. Försök att upprepa "
"kraschen i detta fall, och vänta längre tills kraschdialogrutan visas."

#: ../../reference_manual/dr_minw_debugger.rst:64
msgid ""
"Starting from Krita 3.1 Beta 3 (3.0.92), the external DrMingw JIT debugger "
"is not needed for getting the backtrace."
msgstr ""
"Med början på Krita 3.1 Beta 3 (3.0.92) behövs inte den externa avlusaren "
"DrMingw JIT för att få en bakåtspårning."

#: ../../reference_manual/dr_minw_debugger.rst:67
msgid "Using the Debug Package"
msgstr "Använda avlusningspaketet"

#: ../../reference_manual/dr_minw_debugger.rst:69
msgid ""
"Starting from 3.1 Beta 3, the debug package contains only the debug symbols "
"separated from the executables, so you have to download the portable package "
"separately too (though usually you already have it in the first place.)"
msgstr ""
"Från och med 3.1 Beta 3, innehåller avlusningspaketet bara "
"avlusningssymboler skilda från de körbara filerna, så det portabla paketet "
"måste också laddas ner separat (även om man oftast redan har det från "
"början)."

#: ../../reference_manual/dr_minw_debugger.rst:71
msgid ""
"Links to the debug packages should be available on the release announcement "
"news item on https://krita.org/, along with the release packages. You can "
"find debug packages for any release either in https://download.kde.org/"
"stable/krita for stable releases or in https://download.kde.org/unstable/"
"krita for unstable releases. Portable zip and debug zip are found next to "
"each other."
msgstr ""
"Länkar till avlusningspaket ska finnas tillgängliga i nyhetsartikeln med "
"utgivningskungörelsen på https://krita.org/, tillsammans med "
"utgivningspaketen. Man hittar avlusningspaket för alla utgåvor antingen i "
"https://download.kde.org/stable/krita för stabila utgåvor eller i https://"
"download.kde.org/unstable/ för instabila. Portabla zip-filer och avlusnings-"
"zip-filer finns intill varandra."

#: ../../reference_manual/dr_minw_debugger.rst:72
msgid ""
"Make sure you’ve downloaded the same version of debug package for the "
"portable package you intend to debug / get a better (sort of) backtrace."
msgstr ""
"Säkerställ att samma version av avlusningspaketet har laddats ner som det "
"portabla paketet som ska avlusas / få en bättre (sorts) bakåtspårning."

#: ../../reference_manual/dr_minw_debugger.rst:73
msgid ""
"Extract the files inside the Krita install directory, where the sub-"
"directories `bin`, `lib` and `share` is located, like in the figures below:"
msgstr ""
"Packa upp filerna i Kritas installationsmapp, där undermapparna `bin`, `lib` "
"och `share` finns, som i figuren nedan:"

#: ../../reference_manual/dr_minw_debugger.rst:79
msgid ""
"After extracting the files, check the ``bin`` dir and make sure you see the "
"``.debug`` dir inside. If you don't see it, you probably extracted to the "
"wrong place."
msgstr ""
"Efter att filerna har packats upp, kontrollera mappen ``bin`` och säkerställ "
"att mappen ``.debug`` finns i den. Om den inte finns, har uppackningen "
"troligen gjorts på fel plats."
