# translation of docs_krita_org_general_concepts___file_formats___file_png.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_pngm\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-03-01 14:11+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../general_concepts/file_formats/file_png.rst:1
msgid "The Portable Network Graphics file format in Krita."
msgstr "Súborový formát Portable Network Graphics v Krita."

#: ../../general_concepts/file_formats/file_png.rst:11
#, fuzzy
#| msgid "\\*.png"
msgid "*.png"
msgstr "\\*.png"

#: ../../general_concepts/file_formats/file_png.rst:11
#, fuzzy
#| msgid "\\*.png"
msgid "png"
msgstr "\\*.png"

#: ../../general_concepts/file_formats/file_png.rst:11
msgid "portable network graphics"
msgstr ""

#: ../../general_concepts/file_formats/file_png.rst:17
msgid "\\*.png"
msgstr "\\*.png"

#: ../../general_concepts/file_formats/file_png.rst:19
msgid ""
"``.png``, or Portable Network Graphics, is a modern alternative to :ref:"
"`file_gif` and with that and :ref:`file_jpg` it makes up the three main "
"formats that are widely supported on the internet."
msgstr ""

#: ../../general_concepts/file_formats/file_png.rst:21
msgid ""
"PNG is a :ref:`lossless <lossless_compression>` file format, which means "
"that it is able to maintain all the colors of your image perfectly. It does "
"so at the cost of the file size being big, and therefore it is recommended "
"to try :ref:`file_jpg` for images with a lot of gradients and different "
"colors. Grayscale images will do better in PNG as well as images with a lot "
"of text and sharp contrasts, like comics."
msgstr ""

#: ../../general_concepts/file_formats/file_png.rst:23
msgid ""
"Like :ref:`file_gif`, PNG can support indexed color. Unlike :ref:`file_gif`, "
"PNG doesn't support animation. There have been two attempts at giving "
"animation support to PNG, APNG and MNG, the former is unofficial and the "
"latter too complicated, so neither have really taken off yet."
msgstr ""

#: ../../general_concepts/file_formats/file_png.rst:25
msgid ""
"Since 4.2 we support saving HDR to PNG as according to the `W3C PQ HDR PNG "
"standard <https://www.w3.org/TR/png-hdr-pq/>`_. To save as such files, "
"toggle :guilabel:`Save as HDR image (Rec. 2020 PQ)`, which will convert your "
"image to the Rec 2020 PQ color space and then save it as a special HDR PNG."
msgstr ""
