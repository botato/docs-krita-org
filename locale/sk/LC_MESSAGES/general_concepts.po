# translation of docs_krita_org_general_concepts.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-01 14:37+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../general_concepts.rst:5
msgid "General Concepts"
msgstr "Všeobecné koncepty"

#: ../../general_concepts.rst:7
msgid ""
"Learn about general art and technology concepts that are not specific to "
"Krita."
msgstr ""

#: ../../general_concepts.rst:9
msgid "Contents:"
msgstr "Obsah:"
