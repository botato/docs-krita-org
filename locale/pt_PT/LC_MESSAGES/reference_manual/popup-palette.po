# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-17 15:14+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: images image Kritamouseright alt icons palette popup\n"
"X-POFile-SpellExtra: Krita detail ref mouseright\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../reference_manual/popup-palette.rst:1
msgid "The Pop-up Palette in Krita"
msgstr "A Paleta Instantânea no Krita"

#: ../../reference_manual/popup-palette.rst:10
#: ../../reference_manual/popup-palette.rst:14
msgid "Pop-up Palette"
msgstr "Paleta"

#: ../../reference_manual/popup-palette.rst:16
msgid ""
"The Pop-up Palette is a feature unique to Krita amongst the digital painting "
"applications. It is designed to increase productivity and save time of the "
"artists by providing quick access to some of the most frequently used tools "
"and features in Krita. The Pop-up palette can be accessed by |mouseright| on "
"the canvas. A circular palette similar to what is shown in the image below "
"will spawn at the position your mouse cursor."
msgstr ""
"A Paleta Instantânea é uma funcionalidade única do Krita face às outras "
"aplicações de pintura digital. Está desenhada para aumentar a produtividade "
"e poupar tempo aos artistas, dando o acesso rápido a algumas das ferramentas "
"e funcionalidades usadas com maior frequência no Krita. Poderá aceder à "
"Paleta Instantânea com o |mouseright| sobre a área de desenho. Irá aparecer "
"uma paleta circular semelhante à da imagem abaixo na posição do cursor do "
"seu rato."

#: ../../reference_manual/popup-palette.rst:19
msgid ".. image:: images/popup-palette-detail.svg"
msgstr ".. image:: images/popup-palette-detail.svg"

#: ../../reference_manual/popup-palette.rst:20
msgid ""
"As shown in the image above, the pop-up palette has the following tools and "
"quick access shortcuts integrated into it"
msgstr ""
"Como aparece na imagem acima, a paleta instantânea tem as seguintes "
"ferramentas e atalhos de acesso rápido integrados com ela"

#: ../../reference_manual/popup-palette.rst:22
msgid ""
"Foreground color and Background color indicators on the top left of the "
"palette."
msgstr ""
"Indicadores da Cor Principal e da Cor de fundo na parte superior esquerda da "
"paleta."

#: ../../reference_manual/popup-palette.rst:23
msgid ""
"A canvas rotation circular slider, which can help the artist quickly rotate "
"the canvas while painting."
msgstr ""
"Uma barra circular de rotação da área de desenho, que poderá ajudar o "
"artista a rodar rapidamente a área de desenho enquanto pinta."

#: ../../reference_manual/popup-palette.rst:24
msgid ""
"A group of brush presets, based on the tag selected by the artist. By "
"default the **My Favorite** tag is selected. By default only first 10 "
"presets from the tag are shown, however you can change the number of brush "
"presets shown by changing the value in the :ref:`Miscellaneous Settings "
"Section <misc_settings>` of the dialog box."
msgstr ""
"Um grupo de predefinições de pincéis, baseados na marca seleccionada pelo "
"artista. Por omissão, está seleccionada a marca **Os Meus Favoritos**. Por "
"omissão, só aparecem as primeiras 10 predefinições da marca; contudo, poderá "
"alterar o número de predefinições visíveis, alterando o valor na :ref:"
"`Secção de Configurações Diversas <misc_settings>` da janela."

#: ../../reference_manual/popup-palette.rst:25
msgid ""
"Color Selector with which you can select the hue from the circular ring and "
"lightness and saturation from the triangular area in the middle."
msgstr ""
"Um Selector de Cores com o qual poderá seleccionar a matiz no anel circular "
"e a luminosidade e saturação na área triangular ao meio."

#: ../../reference_manual/popup-palette.rst:26
msgid ""
"Color history area shows the most recent color swatches that you have used "
"while painting."
msgstr ""
"A área do histórico de cores com as cores mais recentes que usou na pintura."

#: ../../reference_manual/popup-palette.rst:27
msgid ""
"The tag list for brush preset will show you the list of both custom and "
"default tags to choose from, selecting a tag from this list will show the "
"corresponding brush presets in the palette."
msgstr ""
"A lista de marcas para as predefinições de pincéis mostrar-lhe-á a lista das "
"marcas personalizadas e predefinidas para escolher; se seleccionar uma marca "
"nesta lista irá mostrar as predefinições de pincéis correspondentes na "
"paleta."

#: ../../reference_manual/popup-palette.rst:28
msgid ""
"The common brush options such as size, opacity, angle et cetera will be "
"shown when you click the **>** icon. A dialog box will appear which will "
"have the sliders to adjust the brush options. You can choose which options "
"are shown in this dialog box by clicking on the settings icon."
msgstr ""
"As opções comuns dos pincéis, como o tamanho, a opacidade, o ângulo, etc. "
"ficarão visíveis quando carregar no ícone **>**. Irá aparecer uma janela que "
"terá as barras para ajustar as opções do pincel. Poderá escolher quais as "
"opções que aparecem nesta janela se carregar no ícone de configurações."

#: ../../reference_manual/popup-palette.rst:29
msgid "The zoom slider allows you to quickly zoom the canvas."
msgstr ""
"A barra de ampliação permite-lhe ampliar rapidamente a área de desenho."

#: ../../reference_manual/popup-palette.rst:30
msgid "The 100% button sets the zoom to the 100% of the image size."
msgstr "O botão de 100% configura a ampliação a 100% do tamanho da imagem."

#: ../../reference_manual/popup-palette.rst:31
msgid ""
"The button with the canvas icon switches to the canvas only mode, where the "
"toolbar and dockers are hidden."
msgstr ""
"O botão com o ícone da área de desenho faz com que mude para o modo apenas "
"da área de desenho, onde a barra de ferramentas e as áreas acopláveis ficam "
"escondidas."

#: ../../reference_manual/popup-palette.rst:32
msgid ""
"The button with the mirror icon mirrors the canvas to help you spot the "
"errors in the painting."
msgstr ""
"O botão com o ícone do espelho gera um espelho da área de desenho para o "
"ajudar a descobrir os erros na pintura."
