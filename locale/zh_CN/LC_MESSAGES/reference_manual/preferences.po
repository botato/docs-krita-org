msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___preferences.pot\n"

#: ../../reference_manual/preferences.rst:1
msgid "Krita's settings and preferences options."
msgstr "Krita 的设置与首选项"

#: ../../reference_manual/preferences.rst:16
msgid "Preferences"
msgstr "首选项"

#: ../../reference_manual/preferences.rst:18
msgid ""
"Krita is highly customizable and makes many settings and options available "
"to customize through the Preferences area. These settings are accessed by "
"going to :menuselection:`Settings --> Configure Krita`. On MacOS, the "
"settings are under the topleft menu area, as you would expect of any program "
"under MacOS."
msgstr ""
"Krita 是高度可定制的，并提供了许多设置和选项，可通过首选项进行自定义。选择 :"
"menuselection:`设置--> 配置 Krita` 来访问这些设置。在 MacOS 上, 设置位于顶部"
"菜单条，就像您对 MacOS 下的任何程序所期望的那样。"

#: ../../reference_manual/preferences.rst:20
msgid ""
"Krita's preferences are saved in the file ``kritarc``. This file is located "
"in ``%LOCALAPPDATA%\\`` on Windows, ``~/.config`` on Linux, and ``~/Library/"
"Preferences`` on OS X. If you would like to back up your custom settings or "
"synchronize them from one computer to another, you can just copy this file. "
"It even works across platforms!"
msgstr ""
"Krita 的偏好设置保存在文件 ``kritarc`` 中。此文件，对于 Windows 而言，位于 ``"
"%LOCALAPPDATA%\\`` ，而对于 Linux 是`` ~/.config`` ，OSX 则在 ``~/Library/"
"Pregerences`` 。如果你想备份您的自定义设置，或是将其从一台计算机同步到另一个"
"上，复制此文件即可。它甚至可以跨平台工作~"

#: ../../reference_manual/preferences.rst:22
msgid ""
"If you have installed Krita through the Windows store, the kritarc file will "
"be in another location:"
msgstr "如果你从 Windows Store 安装 Krita，kritarc 文件会在不同的位置:"

#: ../../reference_manual/preferences.rst:24
msgid ""
":file:`%LOCALAPPDATA%\\\\Packages\\\\49800Krita_{RANDOM STRING}\\\\LocalCache"
"\\\\Local\\\\kritarc`"
msgstr ""
":file:`%LOCALAPPDATA%\\\\Packages\\\\49800Krita_{一组随机字符串}\\"
"\\LocalCache\\\\Local\\\\kritarc`"

#: ../../reference_manual/preferences.rst:27
msgid ""
"Custom shortcuts are saved in a separate file ``kritashortcutsrc`` which can "
"also be backed up in the same way. This is discussed further in the "
"shortcuts section."
msgstr ""
"自定义快捷方式保存在单独的文件 \"kritashortcutsrc\" 中, 也可以以相同的方式进"
"行备份。这将在“快捷方式“一章中进一步讨论。由于文档系统的限制，各个选项按照英"
"文名进行排序。"
