# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Shinjo Park <kde@peremen.name>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-22 16:15+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../user_manual.rst:5
msgid "User Manual"
msgstr "사용자 설명서"

#: ../../user_manual.rst:7
msgid ""
"Discover Krita’s features through an online manual. Guides to help you "
"transition from other applications."
msgstr ""
"Krita의 기능을 온라인 사용자 설명서에서 확인하십시오. 다른 프로그램에서 전환"
"하는 데 도움을 줍니다."

#: ../../user_manual.rst:9
msgid "Contents:"
msgstr "내용:"
